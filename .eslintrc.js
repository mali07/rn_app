module.exports = {
  env: {
    commonjs: true,
    node: true,
    es6: true
  },
  extends: "eslint:recommended",
  parserOptions: {
    ecmaFeatures: {
      experimentalObjectRestSpread: true,
      jsx: true
    },
    sourceType: "module"
  },
  parser: "babel-eslint",
  plugins: ["react", "react-native", "prettier"],
  rules: {
    "indent": ["off", 4],
    "linebreak-style": ["error", "unix"],
    "quotes": ["off", "single"],
    "semi": ["off", "never"],
    "comma-style": ["off", "last"], //方数组元素、变量声明等直接需要逗号隔开
    "comma-dangle": ["error", "never"], //这个规则强制在对象和数组中使用尾随逗号 设置不强制
    "no-unused-vars": 0, //不允许定义的变量在后面的代码中没有被使用到
    "no-use-before-define": 2, //所有的变量都应该先定义后使用 定义之前不使用
    "no-undef": 1,
    "no-console": 1,
    "camelcase": 2, //变量驼峰命名
    "no-const-assign": 2,
    "no-dupe-class-members": 2,
    "no-duplicate-case": 2,
    "no-extra-parens": [2, "functions"],
    "no-self-compare": 2,
    "accessor-pairs": 2,
    "no-unreachable": 1, //禁止有执行不到的代码
    "no-sparse-arrays": 2, //禁止稀疏数组，清除多余的逗号申明  比如[1,,2]
    "dot-notation": "error", //不允许关键字出现在变量中
    "no-redeclare": "error", //不允许重复声明
    "no-return-assign": "error", //不允许在return语句中任务
    "no-func-assign":0,//禁止重新分配function声明
    "constructor-super": 2,
    "new-cap": [
      2,
      {
        //构造函数首字母需要大写
        newIsCap: true,
        capIsNew: false
      }
    ],
    "new-parens": 2, //没有参数时，构造函数也需要添加括号
    "no-array-constructor": 2,
    "no-class-assign": 2,
    "react/jsx-equals-spacing": [0, "always"], //jsx中变量等号两边需要有空格
    "no-cond-assign": 2,
    "valid-typeof": 0, //无效的类型之比较有效
    "prettier/prettier": [
      "error",
      {
        printWidth: 100,//设置单行字符数
        tabWidth: 4,//缩进空格数
        semi: true,//代码行后面需不需要生成分号
        trailingComma: "none",//数组后面最后一个索引要不要添加逗号
        singleQuote: false,//需不需要把双引号格式化成单引号
        bracketSpacing: true,//在对象字面量声明所使用的的花括号后（{）和前（}）输出空格
        jsxBracketSameLine: true,//在多行JSX元素最后一行的末尾添加 > 而使 > 单独一行（不适用于自闭和元素）
        arrowParens:'always',//为单行箭头函数的参数添加圆括号
      }
    ]
  }
};
