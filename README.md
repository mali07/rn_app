## 克隆项目种子工程

$ git clone --depth 1 git@gitlab.58corp.com:ershouche-fe/rn-seed-project.git 项目名字

## 进入种子工程

$ cd 项目名字

## 重置 gitlab 远程仓库地址

$ git remote remove origin
$ git remote add origin git@gitlab.58corp.com:cst-rn/项目名字.git

## 下载依赖文件

$ npm install && git submodule update

## 启动本地服务

$ npm start

## 开发