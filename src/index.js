/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import { StackNavigator } from "react-navigation"; //导航库
import MainScreen from "./view/main";
import Tab from "./view/mainTabNav";
const Navigator = StackNavigator(
    {
        Tab: { screen: Tab }
        // Product:{screen:ProductScreen}
    },

    {
        navigationOptions: {
            headerBackTitle: null,
            headerTintColor: "#333333",
            showIcon: true,
            swipeEnabled: false,
            animationEnabled: false
        },

        mode: "card"
    }
);
export default Navigator;
