import React, { Component } from "react";
import {
    Platform,
    StyleSheet,
    Text,
    TextInput,
    Button,
    Alert,
    ScrollView,
    Image,
    View
} from "react-native";
//import WbcstAPP from "./../../rn-app";
import WbcstAPP from "@/rn-app";
const instructions = Platform.select({
    ios: "Press Cmd+R to reload,\n" + "Cmd+D or shake for dev menu",
    android:
        "Double tap R on your keyboard to reload,\n" + "Shake or press menu button for dev menu"
});

const styles = StyleSheet.create({
    submitBtn: {
        height: 36,
        backgroundColor: "#3385ff",
        marginLeft: 50,
        marginRight: 50,
        marginTop: 50
    },
    container: {
        flex: 1,
        backgroundColor: "#F5FCFF",
        flexDirection: "column",
        justifyContent: "space-between"
    },
    header: {
        flex: 1,
        justifyContent: "flex-start",
        flexDirection: "row",
        alignItems: "center",
        margin: 10
    },
    title: {
        fontSize: 50,
        textAlign: "left",
        //justifyContent:'flex-end',//flex-start、center、flex-end、space-around以及space-between
        flex: 2
        //backgroundColor:'red'
    },
    formArea: {
        flex: 1
    },
    labelContainer: {
        height: 80,
        alignItems: "center",
        flexDirection: "row",
        paddingRight: 5,
        //backgroundColor:'red',
        marginTop: 5
    },
    labelText: {
        width: 80,
        height: 35,
        marginRight: 0,
        justifyContent: "center",
        alignItems: "center"
    },
    inputLabel: {
        //paddingTop: 5,
    },
    inputText: {
        height: 35,
        borderWidth: 1,
        borderColor: "powderblue",
        flex: 1,
        fontSize: 13,
        padding: 4,
        marginRight: 5,
        marginTop: 5
    }
});

class App extends Component {
    constructor(props) {
        super(props);
        this.state = { chepaihao: "", vin: "", engineNum: "" };
    }
    static navigationOptions = {
        //不推荐使用，设置页面导航标题 headerTitle: "违章查询",
        title: "违章查询",
        headerTruncatedBackTitle: "返回",
        gesturesEnabled: true //是否支持滑动返回手势，iOS默认支持，安卓默认关闭
    };
    render() {
        return (
            // <ScrollView>
            <View style={styles.container}>
                <View style={{ height: 100, backgroundColor: "powderblue" }}>
                    <View style={styles.header}>
                        <Text style={{ flex: 1 }} />
                        <Image source={require("../img/icon.jpg")} />
                        <Text style={styles.title}>查违章</Text>
                    </View>
                </View>
                <View style={styles.formArea}>
                    <View style={styles.labelContainer}>
                        <View style={styles.labelText}>
                            <Text style={styles.inputLabel}>车牌号：</Text>
                        </View>
                        <TextInput
                            style={styles.inputText}
                            placeholder="请输入车牌号"
                            onChangeText={(text) => this.setState({ chepaihao: text })}
                        />
                    </View>
                    <View style={styles.labelContainer}>
                        <View style={styles.labelText}>
                            <Text style={styles.inputLabel}>车架号：</Text>
                        </View>
                        <TextInput
                            style={styles.inputText}
                            placeholder="请输入车架号"
                            onChangeText={(text) => this.setState({ vin: text })}
                        />
                    </View>
                    <View style={styles.labelContainer}>
                        <View style={styles.labelText}>
                            <Text style={styles.inputLabel}>发动机号：</Text>
                        </View>
                        <TextInput
                            style={styles.inputText}
                            placeholder="请输入发动机号"
                            onChangeText={(text) => this.setState({ engineNum: text })}
                        />
                    </View>
                    <View style={styles.submitBtn}>
                        <Button
                            onPress={this.onButtonPress}
                            title="提交"
                            color="#fff"
                            accessibilityLabel="Learn more about purple"
                        />
                    </View>
                </View>
                {/* <View style={{ backgroundColor: 'powderblue', paddingTop: 0 }}>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
            <Button
              onPress={this.onButtonPress}
              title="反馈"
              accessibilityLabel="This sounds great!"
            />
            <Button
              onPress={this.onButtonPress}
              title="关于我"
              color="#841584"
              accessibilityLabel="关于我"
            />
          </View>
        </View> */}
            </View>
            // </ScrollView>
        );
    }
    onButtonPress() {
        //Alert.alert("请输入完整车辆信息！");
        WbcstAPP.loadPage({
            url: "www.baidu.com",
            isCloseParent: 0
        });
    }
}
export default App;
