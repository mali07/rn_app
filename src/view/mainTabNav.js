import { StackNavigator, TabNavigator, TabBarBottom } from "react-navigation";
import HomeScreen from "./main";
import MineScreen from "./MinePage";
import TabBarItem from "./../component/tabBarItem";

const Tab = TabNavigator(
    {
        Home: {
            screen: HomeScreen,
            navigationOptions: ({ navigation }) => ({
                tabBarLabel: "首页"
                //   tabBarIcon:({focused,tintColor}) => (
                //     <TabBarItem
                //       tintColor={tintColor}
                //       focused={focused}
                //       normalImage={require('../img/icon.jpg')}
                //       selectedImage={require('../img/icon.jpg')}
                //     />
                //   )
            })
        },

        Mine: {
            screen: MineScreen,
            navigationOptions: ({ navigation }) => ({
                tabBarLabel: "我"
                // tabBarIcon:({focused,tintColor}) => (
                //   <TabBarItem
                //    tintColor={tintColor}
                //     focused={focused}
                //     normalImage={require('../img/icon.jpg')}
                //     selectedImage={require('../img/icon.jpg')}
                //   />
                // )
            })
        }
    },

    {
        tabBarComponent: TabBarBottom,
        tabBarPosition: "bottom",
        swipeEnabled: false,
        animationEnabled: false,
        lazy: true,
        tabBarOptions: {
            activeTintColor: "#06c1ae",
            inactiveTintColor: "#979797",
            style: { backgroundColor: "#ffffff" },
            labelStyle: {
                fontSize: 20 // 文字大小
            }
        }
    }
);

export default Tab;
