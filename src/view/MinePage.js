import React, { Component } from "react";
import {
    Platform,
    StyleSheet,
    Text,
    TextInput,
    Button,
    Alert,
    ScrollView,
    Image,
    View
} from "react-native";
class MinePage extends React.Component {
    render() {
        const { navigate } = this.props.navigation;
        return (
            <Button
                title="自我介绍"
                onPress={() => {
                    navigate("Profile", { name: "Jane" });
                }}
            />
        );
    }
}

export default MinePage;
